<h1>Symfony 3.4 + SonataAdmin 3.31.0 + FOSUserBundle 2.1.2 + SonataUserBundle 4.1</h1>

<h2>Requisitos</h2>
<ul>
<li>PHP 7.1</li>
</ul>

<h2>Acciones que hay que hacer para empezar a trabajar</h2>
<ul>
<li>Crear la base de datos</li>
<li>Configurar el archivo <code>app/config/parameters.yml</code> con los datos de la base de datos creada en el punto anterior</li>
<li>Ejecutar el siguiente comando en la consola para generar las tablas: <code>bin/console doctrine:schema:update --force</code></li>
<li>Ejecutar el siguiente comando en la consola para generar un usuario y poder hacer login: <code>bin/console fos:user:create MYUSER --super-admin</code></li>
<li>Ejecutar el siguiente comando en la consola para generar los archivos CSS, JS, imagenes...: <code>bin/console assets:install --symlink</code></li>
<li>Ejecutar el siguiente comando en la consola para actualizar los paquetes: <code>composer update</code></li>
</ul>